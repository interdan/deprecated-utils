export default function toStringForView(number: number, decimalPoints: number, separator?: string): string;
