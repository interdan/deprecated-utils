export default (number, decimalPoints = 0, separator = '.') => {
  if (typeof (number) === 'undefined' || number === null) {
    return '0';
  }

  const numberValue = parseFloat(number.toString().replace(',', '.'));
  if (Number.isNaN(numberValue)) {
    return '0';
  }

  return numberValue.toFixed(decimalPoints).replace(/\B(?=(\d{3})+(?!\d))/g, separator);
};
