// In rollup both ways are resulting in the same small bundle
// God bless rollups' tree-shaking
// However, in webpack only the second example will not include everything from
// interdan-utils package

// import { capitalize } from 'interdan-utils';
import capitalize from 'interdan-utils/parts/capitalize';

console.log(capitalize('hello'));
